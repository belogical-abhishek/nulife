import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { LoadingController, AlertController } from 'ionic-angular';
/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  //appUrl = 'http://13.126.68.132/nu-life/public';
  appUrl = 'http://192.168.43.246/nu-life/public';
  
  constructor(public http: HttpClient, 
    private fileTransfer: FileTransfer,
    public file: File,
    private alertController: AlertController,
    public loadingController: LoadingController) {
    //console.log('Hello RestServiceProvider Provider');
  }

  presentAlert(title) {
    let alert = this.alertController.create({
      title: 'NuLife',
      subTitle: title,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  download(data) {
    const fileTranser: FileTransferObject = this.fileTransfer.create();
    const url = this.appUrl;
    const downloadFileDirectory = this.file.externalApplicationStorageDirectory;

    let loading = this.loadingController.create({
      content: 'Please wait...'
    });
    
    loading.present();
    data.forEach(element => {
      var filename = element.image_url;
      fileTranser.download(url+"/uploads/"+filename, downloadFileDirectory + filename);
    });
    loading.present();
      setTimeout(() => {
        loading.dismiss();
      }, 5000);
  }

  getCatSubCats() {
    let data = {            
      temp: 'hi'
    }
    let input = JSON.stringify(data);
    
    return new Promise(resolve => {
      this.http.post(this.appUrl+'/api/getCatSubCats', input, {headers: {'Content-Type': 'application/json'}} ).subscribe(res => {
    	  resolve(res);
    	}, err => {
    	  console.log(err);
    	});
  	});
  }

  //search functionality
  getSubCategories(): Observable<string[]> {
    return this.http.get(this.appUrl+'/api/getSubCategories').pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
