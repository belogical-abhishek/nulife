import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Page6 } from './page6';

@NgModule({
  declarations: [
    Page6,
  ],
  imports: [
    IonicPageModule.forChild(Page6),
  ],
})
export class Page6Module {}
