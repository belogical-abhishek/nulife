import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Page9 } from './page9';

@NgModule({
  declarations: [
    Page9,
  ],
  imports: [
    IonicPageModule.forChild(Page9),
  ],
})
export class Page9Module {}
