import { Component } from '@angular/core';
import { NavController,MenuController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

import { NativeStorage } from '@ionic-native/native-storage';

import { AlertController } from 'ionic-angular';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
import { File } from '@ionic-native/file';

declare var $:any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  res: any;
  categories: any;
  subCategories: any;
  appUrl: string;

  subCats: any;
  errorMessage: string;

  descending: boolean = false;
  order: number;
  column: string = 'title';

  isOpen: boolean;

  db: SQLiteObject;

  networkStatus: string;

  constructor(
    public file: File,
    public storage: NativeStorage,
    public navCtrl: NavController,
    public platform: Platform,
    public restProvider: RestProvider,
    public menuCtrl: MenuController,
    public sqlite: SQLite,
    private alertController: AlertController,
    private network: Network
  ) {
     
    // //console.log(this.appUrl);
    // this.getAllCategories();
    //this.getCatSubCats();
    
    // this.createTable();
    
    // this.insertCategories();
    // this.insertSubCategories();

    this.appUrl = restProvider.appUrl;
    this.storage.getItem("categories").then((data) => {
      this.categories = data;
    }, (error) => {
      this.presentAlert("Failed to load saved categories");
    });

    this.storage.getItem("subCategories").then((data) => {
      this.subCategories = data;
    }, (error) => {
      this.presentAlert("Failed to load saved sub categories");
    });

    // this.categories = [
    //   {id: 1, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Medicines"}, 
    //   {id: 2, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Lotions"},
    //   {id: 3, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Syrups"},
    //   {id: 4, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Alopathic"}
    // ];

    // this.subCategories = [
    //   {id: 2, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Crocin", categories_id: 1, image_url: "i-6887.jpg"},
    //   {id: 3, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Adulsa", categories_id: 3, image_url: "1-8494.jpg"},
    //   {id: 4, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Vicks", categories_id: 1, image_url: "2-w-4878.jpg"},
    //   {id: 5, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Vaseline", categories_id: 2, image_url: "3-9355.jpg"},
    //   {id: 6, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Oraheal", categories_id: 3, image_url: "img-1-3411.png"},
    //   {id: 7, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Odomos", categories_id: 2, image_url: "img-2-3946.png"},
    //   {id: 8, created_at:"2018-06-22 08:01:06", updated_at:"2018-06-22 08:01:06", title: "Tuberculinum", categories_id: 4, image_url: "placeholder_male1-3431.jpg"},
    // ];
    //this.getCatSubCats();
  
    this.network.onchange().subscribe(() => {
      this.networkStatus = this.network.type;
    });
  }

  sync() {
    if(this.networkStatus == "none") {
      this.presentAlert("No internet connection found.");
    } else {
      this.getCatSubCats();
    }
  }

  presentAlert(title) {
    let alert = this.alertController.create({
      title: 'NuLife',
      subTitle: title,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  ionViewDidLoad() {
    //$("#flipbook").turn({
    //  width: '100%',
    //  height: this.platform.height() - 50,
    //  display: 'single',
    //  autoCenter: true
    //});

    setTimeout(function(){
      $("#flipbook1").turn({
        width: '100%',
        height: 940,
        display: 'single',
        autoCenter: true
      });  
    },1000);

    this.getSubCategories();  

    this.menuCtrl.swipeEnable(false);
  }

  getCatSubCats() {
    this.restProvider.getCatSubCats()
    .then(data => {
      this.res = data;
      this.categories = this.res.categories;
      this.subCategories = this.res.subCategories;
      this.restProvider.download(this.subCategories);
      
      this.storage.setItem("categories", this.categories);

      this.storage.setItem("subCategories", this.subCategories);

      this.navCtrl.setRoot(this.navCtrl.getActive().component);
    });
  }

  loadPage(pageNo) {
    $("#flipbook1").turn("page", pageNo+1);
    this.hideMenu();
    this.hideFilterList();
  }

  showMenu(){
    $('.menu-box').fadeIn();
  }
  
  hideMenu(){
    $('.menu-box').fadeOut();
  }

  //search
  getSubCategories() {
    this.restProvider.getSubCategories()
       .subscribe(
         subCats => this.subCats = subCats,
         error =>  this.errorMessage = <any>error);
  }

  sort(){
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  onInput(){
    $('.subCategories-list').show();
  }
  
  getPageNo(title){
    let pageNo = this.subCats.findIndex(x => x.title==title)+1;
    this.loadPage(pageNo);
    console.log(this.subCats);
  }

  hideFilterList(){
    $('.subCategories-list').hide();
  }
}
