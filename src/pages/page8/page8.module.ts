import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Page8 } from './page8';

@NgModule({
  declarations: [
    Page8,
  ],
  imports: [
    IonicPageModule.forChild(Page8),
  ],
})
export class Page8Module {}
