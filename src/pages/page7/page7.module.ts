import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Page7 } from './page7';

@NgModule({
  declarations: [
    Page7,
  ],
  imports: [
    IonicPageModule.forChild(Page7),
  ],
})
export class Page7Module {}
