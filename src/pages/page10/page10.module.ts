import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Page10 } from './page10';

@NgModule({
  declarations: [
    Page10,
  ],
  imports: [
    IonicPageModule.forChild(Page10),
  ],
})
export class Page10Module {}
