import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Page11 } from './page11';

@NgModule({
  declarations: [
    Page11,
  ],
  imports: [
    IonicPageModule.forChild(Page11),
  ],
})
export class Page11Module {}
